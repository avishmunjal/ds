package com.ds.practice.tree;

public class DistanceBetweenTwoNodes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
BinaryTree bt=new BinaryTree();

bt.add(1);
bt.add(2);
bt.add(3);
bt.add(4);
bt.add(5);
bt.add(6);
bt.add(7);
bt.add(8);
bt.add(9);
bt.print();
System.out.println(findDistanceBetweentwoNodes(bt.getRoot(),2,7));
	}
	
	
	
public static int findDistanceBetweentwoNodes(Node root,int a,int b) {
	Node lca=LCAInBT.findLCA(root,a,b);
	
	int aDistance=FindDistanceOfNodeinBT.findDistanceOfNode(lca,a,0);
	int bDistance=FindDistanceOfNodeinBT.findDistanceOfNode(lca,b,0);
	
	return aDistance+bDistance;
}

}
