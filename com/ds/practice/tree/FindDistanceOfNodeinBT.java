package com.ds.practice.tree;

public class FindDistanceOfNodeinBT {
	
	
	
	public static int findDistanceOfNode(Node root,int a,int level) {
		
		if(root==null)
			return -1;
		
		if(root.data==a)
			return level;
		
		int leftData=findDistanceOfNode(root.left,a,level+1);
		int rightData=-1;
		if(leftData==-1)
		rightData=findDistanceOfNode(root.right,a,level+1);
		
		if(leftData!=-1)
			return leftData;
		else
			return rightData;
		
		
		
	}

}
