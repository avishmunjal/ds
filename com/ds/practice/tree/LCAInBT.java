package com.ds.practice.tree;

public class LCAInBT {

	public static Node findLCA(Node root,int a,int b) {
		
		if(null==root || root.data==a || root.data==b)
			return root;
		
		
		
		Node left=findLCA(root.left,a,b);
		
		Node right=findLCA(root.right,a,b);
		
		if(null==right && null==left)
			return null;
		
		if(left!=null && right!=null)
			return root;
		
		if(null!=left && null==right)
			return left;
		if(null!=right && null==left)
			return right;
		
		
		
		return root;
		
		
	}
}
